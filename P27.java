/**
 * Created by Виктор on 07.12.2014.
 */

import java.util.*;

public class P27 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rand = new Random();

        int n=sc.nextInt(), m=sc.nextInt();
        int[][] a = new int[n][m];
        int[] b = new int[m];
        int[] c = new int[m];

        for(int i = 0; i<n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = rand.nextInt(20);
                System.out.print("a[" + i + "][" + j + "] = " + a[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i<m; i++) {
            b[i] = rand.nextInt(20);
            System.out.print("b["+i+"] = "+b[i]+" ");
        }

        for(int i=0; i<m; i++) {
            for(int j=0; j<n; j++) {
                c[i] += a[j][i]* b[i];
            }
        }
        System.out.println();
        System.out.println();
        for (int i = 0; i<m; i++) {
            System.out.print("c["+i+"] = "+c[i]+" ");
        }

    }
}
