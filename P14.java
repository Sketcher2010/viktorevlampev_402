/**
 * Created by Виктор on 05.10.2014.
 */

import java.util.*;

public class P14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите кол-во элементов последовательности: ");
        int n=sc.nextInt();

        System.out.print("a[0] = ");
        int x = sc.nextInt(), max2 = x, max = x, s=1;

        for(int i = 1; i<n; i++) {
            System.out.print("a["+i+"] = ");
            x = sc.nextInt();
            if(x>max) {
                max2 = max;
                max = x;
                s=1;
            } else if(x == max2) {
                s++;
            }
        }
        System.out.print(s);
    }
}
