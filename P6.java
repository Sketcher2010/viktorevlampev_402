
import java.util.*;
import java.io.*;

/**
 * Created by Виктор on 16.09.2014.
 */
public class P6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double e = 1e-6, n = 0, s = 0, p = 1;

        do {
            n = Math.sqrt(2+n);
            p *= n/2;
        } while (Math.abs(n / 2 - 1)>e);
        System.out.print(p);
    }
}
