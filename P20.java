/**
 * Created by Виктор on 05.10.2014.
 */

import java.util.*;

public class P20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt(), b=sc.nextInt();
        System.out.print("NOD is "+nod(a, b));
    }
    
    public static int nod(int a, int b) {
        int min, res=1;
        if(a>b) {
            min = b;
        } else {
            min = a;
        }

        for(int i=min; i>1; i--) {
            if(res == 1) {
                if(a % i == 0 && b % i == 0)
                    res = i;
            }
        }
        return res;
    }
}
