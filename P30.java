/**
 * Created by Виктор on 07.12.2014.
 */
import java.util.*;
public class P30 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rand = new Random();

        int n = sc.nextInt();
        int[] a = new int[n];

        for(int i = 0; i<n; i++) {
            a[i] = rand.nextInt(200);
        }
        quicksort(a, 0, n-1);
        for(int i = 0; i<n; i++) {
            System.out.print(a[i]+" ");
        }
    }
    public static int partition (int[] array, int start, int end)
    {
        int marker = start;
        for ( int i = start; i <= end; i++ )
        {
            if ( array[i] <= array[end] )
            {
                int temp = array[marker];
                array[marker] = array[i];
                array[i] = temp;
                marker += 1;
            }
        }
        return marker - 1;
    }

    public static void quicksort(int[] array, int start, int end)
    {
        if ( start >= end )
        {
            return;
        }
        int pivot = partition (array, start, end);
        quicksort (array, start, pivot-1);
        quicksort (array, pivot+1, end);
    }
}
