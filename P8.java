
/**
 * Created by Виктор on 18.09.2014.
 */

public class P8 {
    public static void main(String[] args) {

        double e = 1e-6, n=1, s=3, slog;

        do {
            n += 2;
            slog = 4/((n-1)*n*(n+1));
            s += slog;
        } while (slog>e);
        System.out.print(s);
    }
}
