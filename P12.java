/**
 * Created by Виктор on 05.10.2014.
 */

import java.util.*;

public class P12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n=sc.nextInt(), a, p=0, pp=0, s=0;
        for (int i=0; i<n; i++) {
            System.out.print("a["+i+"] = ");
            a = sc.nextInt();
            if(a>pp && pp<p) {
                s++;
            }
            p=pp;
            pp=a;
        }
        if(pp>p)
            s++;
        System.out.print(s);
    }
}
