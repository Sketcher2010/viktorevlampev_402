/**
 * Created by Виктор on 07.12.2014.
 */

import java.util.*;
import java.io.*;

public class P1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int x = sc.nextInt();
        double s;

        if(x > 5) {
            s = x*x+2*x-7;
        } else if( x <= 1) {
            s = Math.sqrt(x*x-7*x);
        } else {
            s = x*x*x-Math.abs(x)+2;
        }
        System.out.print(s);
    }
}
