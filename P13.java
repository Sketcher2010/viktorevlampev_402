/**
 * Created by Виктор on 05.10.2014.
 */

import java.util.*;

public class P13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n=sc.nextInt(), a, pp=0, s=1, ot1=0, max=0;
        for (int i=0; i<n; i++) {
            System.out.print("a["+i+"] = ");
            a = sc.nextInt();
            if(a<0) {
                if(ot1 == 0) {
                    ot1 = a;
                    max = ot1;
                }
            }
            if(ot1<0) {
                if(a>max) {
                    max = a;
                    s=1;
                    pp=0;
                } else if(a==max) {
                    s++;
                }
                if(a<0) {
                    pp = pp + s;
                }
            }
        }
        System.out.print(pp);
    }
}
