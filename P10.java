
/**
 * Created by Виктор on 19.09.2014.
 */
import java.util.*;
import java.io.*;

public class P10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt(), x = sc.nextInt(), p, pp = 0;

        for (int i=1; i<n; i++) {
            p=x;
            x = sc.nextInt();

            if(pp == 0) {
                if(p>x) {
                    pp = 1; // Если мы дошли до элемента, где последний предпоследний элемент больше, чем текущий, то мы переходим к другому условию
                }
            } else if(pp == 1) {
                if(p<x) {
                    pp = 2;
                    System.out.print("Нет, не является унимодальной!");
                }
            }
        }
        if(pp == 1 || pp == 0)
            System.out.print("Да, является унимодальной!");
    }
}