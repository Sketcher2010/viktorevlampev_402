/**
 * Created by Виктор on 07.12.2014.
 */import java.util.*;

public class P22 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int length = sc.nextInt();
        int i1 = 0, i2 = 0;
        int decVar = 0, incVar = 1;
        int counter = length, chng = 0;
        int[][] a = new int[length][length];

        for (int i = 0; i < length * length; i++) {
            a[i1][i2] = i + 1;
            if (--counter == 0) {
                counter = length - chng / 2 - 1;
                int temp = incVar;
                incVar = -decVar;
                decVar = temp;
                chng++;
            }
            i1 += decVar;
            i2 += incVar;
        }
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++)
                System.out.print(a[i][j] + "    ");
            System.out.println();
        }
    }
}