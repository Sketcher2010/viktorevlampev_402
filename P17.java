/**
 * Created by Виктор on 05.10.2014.
 */

import java.util.*;

public class P17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Сколько будет кол-во элементов в массиве? ");
        int n = sc.nextInt();
        int[] a = new int[n];

        for(int i = 0; i < n; i++) {
            System.out.print("a[" + i + "] = ");
            a[i] = sc.nextInt();
        }
        System.out.print("Введите любое число K: ");
        int k = sc.nextInt(), j = -1;
        for(int i = 0; i < n; i++) {
            if(j == -1)
                if(k == a[i])
                    j = i;
        }
        System.out.println(j);
    }
}
