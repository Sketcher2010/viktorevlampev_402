/**
 * Created by Виктор on 07.12.2014.
 */

import java.util.*;

public class P24 {
    public static void main(String[] args) {
        Scanner zn = new Scanner(System.in);
        int n = zn.nextInt();
        int[][] a = new int[n][n];
        int i, j;
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                a[i][j] = zn.nextInt();
            }
        }
        int rowSum1 = 0;
        int rowSum2 = 0;
        i = 0;
        while ((i < n - 1) && (rowSum1 == rowSum2)) {
            rowSum1 = 0;
            rowSum2 = 0;
            for (j = 0; j < n; j++) {
                rowSum1 += a[i][j];
                rowSum2 += a[i + 1][j];
            }
            i++;
        }
        boolean rowsEq = false;
        if ((i == n - 1) && (rowSum1 == rowSum2)) {
            rowsEq = true;
        }
        int colSum1 = 0;
        int colSum2 = 0;
        j = 0;
        while ((j < n - 1) && (colSum1 == colSum2)) {
            colSum1 = 0;
            colSum2 = 0;
            for (i = 0; i < n; i++) {
                colSum1 += a[i][j];
                colSum2 += a[i][j + 1];
            }
            j++;
        }
        boolean colsEq = false;
        if ((j == n - 1) && (colSum1 == colSum2)) {
            colsEq = true;
        }
        int d1 = 0;
        for (i = 0; i < n; i++) {
            d1 += a[i][i];
        }
        int d2 = 0;
        for (i = 0; i < n; i++) {
            d2 += a[i][n - 1 - i];
        }
        if (rowsEq && colsEq && (rowSum1 == colSum1) && (colSum1 == d1) && (d1 == d2)) {
            System.out.print("Magic!");
        } else {
            System.out.print("Not Magic.");
        }
    }
}