/**
 * Created by Виктор on 14.09.2014.
 */
import java.util.*;

public class P3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double e = 1e-6, x = sc.nextDouble(), n = 1, s = x, slog, fac=1, ch = x;

        do {
            ch *= -x*x;
            fac *= (n+1)*(n+2);
            slog = ch/fac;
            s += slog;
            n+=2;
        } while (Math.abs(slog) > e);
        System.out.print(s);
    }
}
// Простите за индусский код, пожалуйста :3