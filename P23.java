/**
 * Created by Виктор on 07.12.2014.
 */

import java.util.*;

public class P23 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int length = sc.nextInt();
        int i1 = 0, i2 = 0;
        int decVar = 0, incVar = 1;
        int counter = length, chng = 0, q = 1;
        int[][] a = new int[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++)
                a[i][j] = 0;
            q += i;
        }
        for (int i = 0; i < q; i++) {
            a[i1][i2] = i + 1;
            System.out.println(i1 + " " + i2);
            if (--counter == 0) {
                counter = (length - chng / 2 - 1) / 2;
                switch (decVar) {
                    case 0:
                        decVar = 1;
                        incVar = 0;
                    case 1:
                        decVar = -1;
                        incVar = -1;
                    case -1:
                        decVar = 0;
                        incVar = 1;
                        chng++;
                }
            }
            i1 += decVar;
            i2 += incVar;
        }
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++)
                System.out.print(a[i][j] + "\u0009");
            System.out.println();
        }
    }
}