/**
 * Created by Виктор on 14.09.2014.
 */
import java.util.*;

public class P4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter num N: ");
        int n = sc.nextInt(), result = 0, max = n;

        System.out.print("To logging? (true or false) ");
        boolean logs = sc.nextBoolean();
        if(logs == true)
            System.out.print(n);
        while (n != 1) {
            if(n%2 == 1) {
                n = 3*n+1;
                result++;
                if(n > max)
                    max = n;
                if(logs == true)
                    System.out.print(" -> " + n);
            } else {
                n /= 2;
                result++;
                if(n > max)
                    max = n;
                if(logs == true)
                    System.out.print(" -> " + n);
            }
        }
        if(logs == true)
            System.out.println();
        System.out.print("Result: " + result + ", max: " + max);
    }
}