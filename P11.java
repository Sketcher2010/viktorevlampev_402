
/**
 * Created by Виктор on 25.09.2014.
 */

import java.util.*;

public class P11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int k = sc.nextInt(), n=sc.nextInt(), a, max=0, s=0;
        for (int i=0; i<n; i++) {
            System.out.print("a["+i+"] = ");
            a = sc.nextInt();
            if(a>max) {
                max = a;
                s = 1;
            } else if(a == max) {
                s++;
            }
        }
        if(k == s)
            System.out.print("Равно");
        else
            System.out.print("Не равно");
    }
}
